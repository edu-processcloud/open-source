program Shocker;

uses
  Forms,
  Analiza in 'ANALIZA.PAS' {frmAnalizador},
  Getresis in 'GETRESIS.PAS' {frmGetResistor},
  Resistor in 'RESISTOR.PAS',
  Conector in 'CONECTOR.PAS',
  Linha in 'LINHA.PAS',
  Ligacao in 'LIGACAO.PAS',
  Getfont in 'GETFONT.PAS' {frmGetFonte},
  Funciona in 'FUNCIONA.PAS',
  Fonte in 'FONTE.PAS',
  Salva in 'SALVA.PAS',
  Abre in 'ABRE.PAS',
  Coment in 'COMENT.PAS' {frmProjeto},
  Funcoes in 'FUNCOES.PAS',
  Equip in 'EQUIP.PAS' {Equipe},
  erro in 'erro.pas' {frmErro};

{$R *.RES}

begin
  Application.Title := 'Shocker ';
  Application.CreateForm(TfrmAnalizador, frmAnalizador);
  Application.CreateForm(TfrmGetResistor, frmGetResistor);
  Application.CreateForm(TfrmGetFonte, frmGetFonte);
  Application.CreateForm(TfrmProjeto, frmProjeto);
  Application.CreateForm(TEquipe, Equipe);
  Application.CreateForm(TfrmErro, frmErro);
  Application.Run;
end.
