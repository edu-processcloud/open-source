unit erro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, GIFImage, ExtCtrls;

type
  TfrmErro = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    GIFImage1: TGIFImage;
    Image1: TImage;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmErro: TfrmErro;

implementation

{$R *.DFM}

procedure TfrmErro.Button1Click(Sender: TObject);
begin
     frmErro.close;
end;

procedure TfrmErro.Button2Click(Sender: TObject);
begin
     halt(1);
end;

procedure TfrmErro.Button3Click(Sender: TObject);
begin
     exitwindows( 0, EWX_REBOOT );
end;

procedure TfrmErro.Button4Click(Sender: TObject);
begin
     messagedlg( 'N�o � possivel formatar a unidade "C"', mtError, [ mbOK ], 0 ); 
end;

procedure TfrmErro.FormActivate(Sender: TObject);
begin
     Gifimage1.LoadFromFile( 'dmfire.gif');
     GifImage1.Animate := true;
end;

end.
