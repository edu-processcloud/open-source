/*
 Fade

 This example shows how to fade an LED on pin 9
 using the analogWrite() function.

 This example code is in the public domain.
 */

int led = 13;           // the pin that the LED is attached to
int motor = 6;           // the pin that the LED is attached to
int brightness = 0;    // how bright the LED is
int fadeAmount = 100;    // how many points to fade the LED by

// the setup routine runs once when you press reset:
void setup() {
  // declare pin 9 to be an output:
  pinMode(led, OUTPUT);
  pinMode(motor, OUTPUT);
  randomSeed(analogRead(0));
  analogWrite(motor, 255);
  delay(1000);

}

// the loop routine runs over and over again forever:
void loop() {

  analogWrite(motor, fadeAmount);
  analogWrite(led, fadeAmount);
  delay((random(500, 10000)/fadeAmount)*2);
  fadeAmount --;
   if (fadeAmount == 45) {
    fadeAmount = 100 ;
    delay((random(140, 5000)));
    delay((random(900)));
  }
  if( random(200) ==18){
    fadeAmount = 255;
    analogWrite(motor, fadeAmount);
    delay(1000);
  } 
}

