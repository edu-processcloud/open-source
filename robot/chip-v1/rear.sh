#!/bin/bash

if [ -t 0 ]; then stty -echo -icanon -icrnl time 0 min 0; fi

count1=0
count=0
keypress=''
while [ "x$keypress" = "x" ]; do
  sudo sh -c 'echo 1 > /sys/class/gpio/gpio1019/value'
  sudo sh -c 'echo 1 > /sys/class/gpio/gpio1017/value'

  sleep 0.000005
  sudo sh -c 'echo 0 > /sys/class/gpio/gpio1019/value'
  sudo sh -c 'echo 0 > /sys/class/gpio/gpio1017/value'
  sleep 0.0015
  let count+=1
  echo -ne $count'\r'
  keypress="`cat -v`"
done

if [ -t 0 ]; then stty sane; fi

echo "You pressed '$keypress' after $count loop iterations"
echo "Thanks for using this script."
sudo sh -c 'echo 0 > /sys/class/gpio/gpio1019/value'
sudo sh -c 'echo 0 > /sys/class/gpio/gpio1017/value'

exit 0
